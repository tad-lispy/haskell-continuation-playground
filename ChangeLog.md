# Changelog for Haskell Continuation Playground

## Unreleased changes

### Added
  * A printf function with variadic arguments based on continuation

    Following excellent https://dbp.io/talks/2016/fn-continuations-haskell-meetup.pdf

  * Type annotation for `(%)`
    
    Thanks to `:type` of `ghci`. Learned from http://dev.stephendiehl.com/hask/
