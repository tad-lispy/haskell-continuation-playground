# Haskell Continuation Playground

Just me learning Haskell Functional-Fu and getting 🤯 about it.

## TODO:

  - [ ] Move implementation of `printf`, `s`, `c`, `d`, `(%)` to `src/`. 
  - [ ] Implement tests in `tests/Spec.hs`.
  - [x] What's the type of `(%)`?
  - [ ] Describe how to setup and run this project in `README`
