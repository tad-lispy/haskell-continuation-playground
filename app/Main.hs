module Main where

main :: IO ()
main = do
  printf greeting "Tad" 42
  printf about "Fana" "George" "Appeltaart" "Chilly"
  printf greeting "Fana" 47
  printf about "Tad" "George" "Chilly" "Appeltaart"

printf :: ((String -> IO ()) -> a) -> a
printf format = format putStrLn

{- Constant string -}
c :: String -> (String -> a) -> a
c string continuation = continuation string

d :: (String -> a) -> Int -> a
d continuation int = continuation (show int)

s :: (String -> a) -> String -> a
s continuation string = continuation string

(%) :: ((String -> b) -> a) -> ((String -> c) -> b) -> (String -> c) -> a
f1 % f2 = \continuation -> f1 (\s1 -> f2 (\s2 -> continuation (s1 ++ s2)))

greeting = c "Hello! I am " % s % c ". I have " % d % c " cats! 🙀"
about =
  c "I like " % s % c " and " % s % c " and " % s % c " but not " % s % c "!"
